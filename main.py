from fastapi import FastAPI
import uvicorn
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    "*",
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

from controllers import root_web_controller
root_web_controller.init_static_web_conf(app=app)
app.include_router(root_web_controller.router)
# app.include_router(safety_audit_controller.router)
# app.include_router(media_controller.router)
from controllers import pure_rtc_controller
app.include_router(pure_rtc_controller.router)
from controllers import video_2_way_controller
app.include_router(video_2_way_controller.router)
from controllers import common_rtc_controller
app.include_router(common_rtc_controller.router)
from controllers import video_subscribe_controller
app.include_router(video_subscribe_controller.router)
from controllers import audio_2_way_controller
app.include_router(audio_2_way_controller.router)
from controllers import audio_subscribe_controller
app.include_router(audio_subscribe_controller.router)

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=3000, reload=True)