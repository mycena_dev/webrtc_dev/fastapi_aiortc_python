from pydantic import BaseModel

class Offer(BaseModel):
    type: str
    sdp: str
