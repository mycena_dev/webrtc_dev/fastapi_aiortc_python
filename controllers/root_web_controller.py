from fastapi import FastAPI, Request
from fastapi import APIRouter
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from starlette.responses import FileResponse

templates = Jinja2Templates(directory="dist")

router: APIRouter = APIRouter(
    prefix="",
    tags=["web"],
)

def init_static_web_conf(app: FastAPI):
    app.mount("/_app", StaticFiles(directory="dist/_app", html = True), name="_app")

@router.get("/")
async def example(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@router.get("/video-subscriber")
async def example(request: Request):
    return templates.TemplateResponse("video-subscriber.html", {"request": request})

@router.get("/audio-2-way")
async def example(request: Request):
    return templates.TemplateResponse("audio-2-way.html", {"request": request})

@router.get("/pure-rtc")
async def example(request: Request):
    return templates.TemplateResponse("pure-rtc.html", {"request": request})

@router.get("/audio-subscriber")
async def example(request: Request):
    return templates.TemplateResponse("audio-subscriber.html", {"request": request})

@router.get("/video-2-way")
async def example(request: Request):
    return templates.TemplateResponse("video-2-way.html", {"request": request})

@router.get('/favicon.png')
async def favicon():
    return FileResponse('dist/favicon.png')