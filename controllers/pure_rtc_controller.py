import random
from fastapi import APIRouter
from aiortc import RTCPeerConnection, RTCSessionDescription, RTCConfiguration
from aiortc.rtcdatachannel import RTCDataChannel
from domains.offer import Offer
from store.web_rtc_store import WebRTCStore

router: APIRouter = APIRouter(
    prefix="",
    tags=["pure-rtc"],
)

sentence_list = ["Hello!", "Any question?", "This is a good day!", "ByeBye <3"]

@router.post("/pure-offer")
async def offer(params: Offer):
    offer = RTCSessionDescription(sdp=params.sdp, type=params.type)

    config: RTCConfiguration = RTCConfiguration()
    config.iceServers = [
                {
                    "urls": ['stun:stun.l.google.com:19302',
                            "stun:stun1.l.google.com:19302",
                            "stun:stun2.l.google.com:19302",
                            "stun:stun3.l.google.com:19302",
                            "stun:stun4.l.google.com:19302",
                    ]
                }, {
                    "urls": ["turn:59.127.60.115:3478"],
                    "username": "mycena",
                    "credential": "mycena"
                }
            ]
    pc = RTCPeerConnection()

    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        if pc.connectionState == "new":
            print(f'{pc} is created.')
        elif pc.connectionState == "connected":
            print(f'{pc} is connected.')
            WebRTCStore.pcs.add(pc)
        elif pc.connectionState == "connecting":
            print(f'{pc} is connecting.')
        elif pc.connectionState == "closed":
            await pc.close()
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is closed.')
        elif pc.connectionState == "failed":
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is failed.')


    @pc.on("datachannel")
    def on_datachannel(channel: RTCDataChannel):
        print(channel, "-", "created by remote party")
        @channel.on("message")
        async def on_message(message):
            if(message == "close"):
                await pc.close()
            else:
                channel.send(random.choice(sentence_list))

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        print("ICE State: ", pc.iceConnectionState)
        discard_state = ["closed", "failed"]
        if pc.iceConnectionState in discard_state:
            WebRTCStore.pcs.discard(pc)
        
    # handle offer
    await pc.setRemoteDescription(offer)

    # send answer
    answer = await pc.createAnswer()

    await pc.setRemoteDescription(offer)

    await pc.setLocalDescription(answer)

    return {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}