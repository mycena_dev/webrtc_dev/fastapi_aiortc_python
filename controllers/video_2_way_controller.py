from fastapi import APIRouter
from typing import Set
from aiortc import RTCPeerConnection, RTCSessionDescription, MediaStreamTrack
from aiortc.rtcdatachannel import RTCDataChannel
from domains.offer import Offer
from store.web_rtc_store import WebRTCStore
from aiortc.contrib.media import MediaRelay
import cv2
from av import VideoFrame

router: APIRouter = APIRouter(
    prefix="",
    tags=["video-2-way"],
)

class VideoTransformTrack(MediaStreamTrack):
    """
    A video stream track that transforms frames from an another track.
    """

    kind = "video"

    def __init__(self, track):
        super().__init__()
        self.track = track

    async def recv(self):
        frame = await self.track.recv()
        img = frame.to_ndarray(format="bgr24")

        # prepare color
        img_color = cv2.pyrDown(cv2.pyrDown(img))
        for _ in range(6):
            img_color = cv2.bilateralFilter(img_color, 9, 9, 7)
        img_color = cv2.pyrUp(cv2.pyrUp(img_color))

        # prepare edges
        img_edges = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        img_edges = cv2.adaptiveThreshold(
            cv2.medianBlur(img_edges, 7),
            255,
            cv2.ADAPTIVE_THRESH_MEAN_C,
            cv2.THRESH_BINARY,
            9,
            2,
        )
        img_edges = cv2.cvtColor(img_edges, cv2.COLOR_GRAY2RGB)

        # combine color and edges
        img = cv2.bitwise_and(img_color, img_edges)

        # rebuild a VideoFrame, preserving timing information
        new_frame = VideoFrame.from_ndarray(img, format="bgr24")
        new_frame.pts = frame.pts
        new_frame.time_base = frame.time_base
        return new_frame

@router.post("/video-2-way")
async def offer(params: Offer):
    offer = RTCSessionDescription(sdp=params.sdp, type=params.type)

    pc = RTCPeerConnection()

    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        if pc.connectionState == "new":
            print(f'{pc} is created.')
        elif pc.connectionState == "connected":
            print(f'{pc} is connected.')
            WebRTCStore.pcs.add(pc)
        elif pc.connectionState == "connecting":
            print(f'{pc} is connecting.')
        elif pc.connectionState == "closed":
            await pc.close()
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is closed.')
        elif pc.connectionState == "failed":
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is failed.')


    @pc.on("datachannel")
    def on_datachannel(channel: RTCDataChannel):
        print(channel, "-", "created by remote party")
        @channel.on("message")
        async def on_message(message):
            if(message == "close"):
                await pc.close()

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        print("ICE State: ", pc.iceConnectionState)
        discard_state = ["closed", "failed"]
        if pc.iceConnectionState in discard_state:
            WebRTCStore.pcs.discard(pc)
    
    @pc.on("track")
    def on_track(track):
        relay = MediaRelay()
        if track.kind == "video":
            pc.addTrack(
                VideoTransformTrack(relay.subscribe(track, buffered=False))
            )
        
    # handle offer
    await pc.setRemoteDescription(offer)

    # send answer
    answer = await pc.createAnswer()

    await pc.setRemoteDescription(offer)

    await pc.setLocalDescription(answer)

    return {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}