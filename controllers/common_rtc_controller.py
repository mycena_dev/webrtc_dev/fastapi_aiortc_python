from fastapi import APIRouter
from store.web_rtc_store import WebRTCStore

router: APIRouter = APIRouter(
    prefix="",
    tags=["common-rtc"],
)

@router.get("/all-pcs")
async def all_pcs():
    return len(WebRTCStore.pcs)