import random
from fastapi import APIRouter
from aiortc import RTCPeerConnection, RTCSessionDescription, VideoStreamTrack
from aiortc.rtcdatachannel import RTCDataChannel
from domains.offer import Offer
from store.web_rtc_store import WebRTCStore
import cv2
from av import VideoFrame
import urllib.request
import json
import numpy as np
import time


router: APIRouter = APIRouter(
    prefix="",
    tags=["video-subscribe"],
)

all_camera_url = []

with open('static/camera.json', 'r', encoding="utf-8") as f:
    data = json.load(f)

    for key, value in data.items(): # 1815
        for camera in value:
            if len(all_camera_url) > 20:
                break
            all_camera_url.append(camera['url'])

class MyVideoTrack(VideoStreamTrack):
    """
    A video stream track that transforms frames from an another track.
    """

    kind = "video"

    def __init__(self, url):
        super().__init__()
        self.url = url
        self.stream = None
        self._bytes = bytes()
        frame = VideoFrame(width=640, height=480)
        for p in frame.planes:
            p.update(bytes(p.buffer_size))
        self.frame = frame

    async def recv(self):
        pts, time_base = await self.next_timestamp()
        try:
            while True:
                self._bytes += self.stream.read(1024)
                a = self._bytes.find(b'\xff\xd8')
                b = self._bytes.find(b'\xff\xd9')

                if a != -1 and b != -1:
                    jpg =self. _bytes[a:b+2]
                    self._bytes =self._bytes[b+2:]
                    i = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
                    frame = VideoFrame.from_ndarray(i, format="bgr24")
                    frame.pts = pts
                    frame.time_base = time_base
                    self.frame = frame
                    return self.frame
        except Exception as e:
            self.stream = urllib.request.urlopen(self.url)
            self._bytes = bytes()
            for p in self.frame.planes:
                p.update(bytes(p.buffer_size))
            self.frame.pts = pts
            self.frame.time_base = time_base
            return self.frame

@router.post("/video-subscribe")
async def offer(params: Offer):
    ct = time.time()
    offer = RTCSessionDescription(sdp=params.sdp, type=params.type)
    print("RTCSessionDescription Cost time: ", time.time() - ct)
    
    pc = RTCPeerConnection()

    for _ in range(offer.sdp.title().count("M=Video")):
        pc.addTransceiver("video")

    # ct = time.time()
    use_url = random.sample(all_camera_url, offer.sdp.title().count("M=Video"))
    # 不要再 onTrack 的 callBack 中做 assign track的動作，會沒有依據要更新哪個 Sender 的 Track。  
    # 所以直接在pc.setLocalDescription(answer)之前找地方分配該有的Track即可
    # 註: onTrack中的track.id是receiver中的track.id
    for i, v in enumerate(pc.getSenders()):
        # ctv = time.time()
        videoTrack = MyVideoTrack(use_url[i])
        # print("new MyVideoTrack Cost time: ", time.time() - ctv)
        v.replaceTrack(videoTrack)
    # print("replaceTrack Cost time: ", time.time() - ct)


    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        if pc.connectionState == "new":
            print(f'{pc} is created.')
        elif pc.connectionState == "connected":
            print(f'{pc} is connected.')
            WebRTCStore.pcs.add(pc)
        elif pc.connectionState == "connecting":
            print(f'{pc} is connecting.')
        elif pc.connectionState == "closed":
            await pc.close()
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is closed.')
        elif pc.connectionState == "failed":
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is failed.')


    @pc.on("datachannel")
    def on_datachannel(channel: RTCDataChannel):
        print(channel, "-", "created by remote party")
        @channel.on("message")
        async def on_message(message):
            if(message == "close"):
                await pc.close()

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        print("ICE State: ", pc.iceConnectionState)
        discard_state = ["closed", "failed"]
        if pc.iceConnectionState in discard_state:
            WebRTCStore.pcs.discard(pc)

    @pc.on("icecandidateerror")
    async def on_icecandidateerrorchange(event):
        print("ICE event: ", event)
        # discard_state = ["closed", "failed"]
        # if pc.iceConnectionState in discard_state:
        #     WebRTCStore.pcs.discard(pc)

    # handle offer
    ct = time.time()
    await pc.setRemoteDescription(offer)
    print("setRemoteDescription Cost time: ", time.time() - ct)
    
    answer = await pc.createAnswer()

    ct = time.time()
    await pc.setLocalDescription(answer)
    print("setLocalDescription Cost time: ", time.time() - ct)


    # print("=======Receiver=======",  len(pc.getReceivers()))
    # for i in pc.getReceivers():
    #     print(i._RTCRtpReceiver__kind, end="")
    #     if i.track is not None:
    #         print()
    #         print(i.track.id)
    #     else: 
    #         print(": No track")

    # print("=======Sender=======",  len(pc.getSenders()))
    # for i in pc.getSenders():
    #     # print(i._RTCRtpSender__kind, end="")
    #     print(i.__dict__)
    #     if i.track is not None:
    #         print()
    #         print(i.track.id)
    #     else: 
    #         print(": No track")

    # print("=======Transceiver=======",  len(pc.getTransceivers()))
    # for i in pc.getTransceivers():
    #     print(i._RTCRtpTransceiver__kind)
    #     print(i.__dict__)


    return {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}
