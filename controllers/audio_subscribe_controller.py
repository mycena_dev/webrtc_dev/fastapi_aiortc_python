import random
from fastapi import APIRouter
from aiortc import RTCPeerConnection, RTCSessionDescription, VideoStreamTrack
from aiortc.rtcdatachannel import RTCDataChannel
from aiortc.contrib.media import MediaPlayer
from domains.offer import Offer
from store.web_rtc_store import WebRTCStore
import cv2
from av import VideoFrame
import urllib.request
import numpy as np
import time

router: APIRouter = APIRouter(
    prefix="",
    tags=["audio_subscribe"],
)

all_audio_url = [
"https://cdn.freesound.org/previews/4/4914_4948-lq.mp3",
"https://cdn.freesound.org/previews/17/17130_4948-lq.mp3",
"https://cdn.freesound.org/previews/6/6164_4948-lq.mp3",
"https://cdn.freesound.org/previews/6/6722_4948-lq.mp3",
"https://cdn.freesound.org/previews/172/172561_45941-lq.mp3",
"https://cdn.freesound.org/previews/2/2790_4948-lq.mp3",
"https://cdn.freesound.org/previews/163/163485_215874-lq.mp3",
"https://cdn.freesound.org/previews/278/278903_3546642-lq.mp3",
"https://cdn.freesound.org/previews/3/3718_4948-lq.mp3",
"https://cdn.freesound.org/previews/4/4359_4948-lq.mp3",
"https://cdn.freesound.org/previews/322/322589_2297168-lq.mp3",
"https://cdn.freesound.org/previews/455/455109_5794612-lq.mp3",
"https://cdn.freesound.org/previews/268/268226_5078136-lq.mp3",
"https://cdn.freesound.org/previews/331/331589_5820980-lq.mp3",
"https://cdn.freesound.org/previews/344/344276_2776777-lq.mp3",
"https://cdn.freesound.org/previews/268/268227_5078136-lq.mp3",
"https://cdn.freesound.org/previews/456/456769_9514571-lq.mp3",
"https://cdn.freesound.org/previews/264/264283_4967729-lq.mp3",
"https://cdn.freesound.org/previews/187/187470_3021251-lq.mp3",
"https://cdn.freesound.org/previews/397/397092_3287894-lq.mp3",
"https://cdn.freesound.org/previews/34/34892_305530-lq.mp3",
"https://cdn.freesound.org/previews/147/147994_2394245-lq.mp3",
"https://cdn.freesound.org/previews/85/85344_1241962-lq.mp3",
"https://cdn.freesound.org/previews/254/254819_4597795-lq.mp3"
]


class MyVideoTrack(VideoStreamTrack):
    """
    A video stream track that transforms frames from an another track.
    """

    kind = "audio"

    def __init__(self, url):
        super().__init__()
        self.url = url
        self.stream = None
        self._bytes = bytes()
        frame = VideoFrame(width=640, height=480)
        for p in frame.planes:
            p.update(bytes(p.buffer_size))
        self.frame = frame

    async def recv(self):
        pts, time_base = await self.next_timestamp()
        try:
            while True:
                self._bytes += self.stream.read(1024)
                a = self._bytes.find(b'\xff\xd8')
                b = self._bytes.find(b'\xff\xd9')

                if a != -1 and b != -1:
                    jpg =self. _bytes[a:b+2]
                    self._bytes =self._bytes[b+2:]
                    i = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
                    frame = VideoFrame.from_ndarray(i, format="bgr24")
                    frame.pts = pts
                    frame.time_base = time_base
                    self.frame = frame
                    return self.frame
        except Exception as e:
            self.stream = urllib.request.urlopen(self.url)
            self._bytes = bytes()
            for p in self.frame.planes:
                p.update(bytes(p.buffer_size))
            self.frame.pts = pts
            self.frame.time_base = time_base
            return self.frame

@router.post("/audio-subscribe")
async def offer(params: Offer):
    ct = time.time()
    offer = RTCSessionDescription(sdp=params.sdp, type=params.type)
    print("RTCSessionDescription Cost time: ", time.time() - ct)
    
    pc = RTCPeerConnection()

    for _ in range(offer.sdp.title().count("M=Audio")):
        pc.addTransceiver("audio")

    # ct = time.time()
    use_url = random.sample(all_audio_url, offer.sdp.title().count("M=Audio"))
    # 不要再 onTrack 的 callBack 中做 assign track的動作，會沒有依據要更新哪個 Sender 的 Track。  
    # 所以直接在pc.setLocalDescription(answer)之前找地方分配該有的Track即可
    # 註: onTrack中的track.id是receiver中的track.id
    for i, v in enumerate(pc.getSenders()):
        videoTrack = MediaPlayer(use_url[i], loop=True)
        v.replaceTrack(videoTrack.audio)


    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        if pc.connectionState == "new":
            print(f'{pc} is created.')
        elif pc.connectionState == "connected":
            print(f'{pc} is connected.')
            WebRTCStore.pcs.add(pc)
        elif pc.connectionState == "connecting":
            print(f'{pc} is connecting.')
        elif pc.connectionState == "closed":
            await pc.close()
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is closed.')
        elif pc.connectionState == "failed":
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is failed.')


    @pc.on("datachannel")
    def on_datachannel(channel: RTCDataChannel):
        print(channel, "-", "created by remote party")
        @channel.on("message")
        async def on_message(message):
            if(message == "close"):
                await pc.close()

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        print("ICE State: ", pc.iceConnectionState)
        discard_state = ["closed", "failed"]
        if pc.iceConnectionState in discard_state:
            WebRTCStore.pcs.discard(pc)

    @pc.on("icecandidateerror")
    async def on_icecandidateerrorchange(event):
        print("ICE event: ", event)
        # discard_state = ["closed", "failed"]
        # if pc.iceConnectionState in discard_state:
        #     WebRTCStore.pcs.discard(pc)

    # handle offer
    ct = time.time()
    await pc.setRemoteDescription(offer)
    print("setRemoteDescription Cost time: ", time.time() - ct)
    
    answer = await pc.createAnswer()

    ct = time.time()
    await pc.setLocalDescription(answer)
    print("setLocalDescription Cost time: ", time.time() - ct)

    return {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}
