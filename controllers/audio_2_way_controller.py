import asyncio
import time
from fastapi import APIRouter
from aiortc import RTCPeerConnection, RTCSessionDescription, MediaStreamTrack
from aiortc.rtcdatachannel import RTCDataChannel
from domains.offer import Offer
from store.web_rtc_store import WebRTCStore
from av import AudioFrame, AudioFifo
import fractions

router: APIRouter = APIRouter(
    prefix="",
    tags=["video-2-way"],
)

class AudioTransformTrack(MediaStreamTrack):
    """
    A video stream track that transforms frames from an another track.
    """

    kind = "audio"

    def __init__(self, track):
        super().__init__()
        self.track = track
        self.audio_buffer = AudioFifo()

    async def recv(self):
        # https://pyav.org/docs/develop/api/audio.html#module-av.audio.frame
        frame: AudioFrame = await self.track.recv()
        sample_rate = frame.sample_rate
        self.audio_buffer.write(frame)
        try:
            if self.audio_buffer.samples > frame.samples*30:
                new_frame = self.audio_buffer.read(frame.samples)
                new_frame.pts = frame.pts
                new_frame.sample_rate = frame.sample_rate
                new_frame.time_base = frame.time_base
                return new_frame
            else:
                samples = frame.samples

                if hasattr(self, "_timestamp"):
                    self._timestamp += samples
                    wait = self._start + (self._timestamp / sample_rate) - time.time()
                    await asyncio.sleep(wait)
                else:
                    self._start = time.time()
                    self._timestamp = 0

                new_frame = AudioFrame(format="s16", layout="stereo", samples=samples)
                new_frame.pts = self._timestamp
                new_frame.sample_rate = sample_rate
                new_frame.time_base = fractions.Fraction(1, sample_rate)
                return new_frame
        except Exception as e:
            print(e)
        samples = frame.samples
        if hasattr(self, "_timestamp"):
            self._timestamp += samples
            wait = self._start + (self._timestamp / sample_rate) - time.time()
            await asyncio.sleep(wait)
        else:
            self._start = time.time()
            self._timestamp = 0
        new_frame = AudioFrame(format="s16", layout="stereo", samples=samples)
        new_frame.pts = self._timestamp
        new_frame.sample_rate = sample_rate
        new_frame.time_base = fractions.Fraction(1, sample_rate)
        return new_frame   


@router.post("/audio-2-way")
async def offer(params: Offer):
    offer = RTCSessionDescription(sdp=params.sdp, type=params.type)
    pc = RTCPeerConnection()

    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        if pc.connectionState == "new":
            print(f'{pc} is created.')
        elif pc.connectionState == "connected":
            print(f'{pc} is connected.')
            WebRTCStore.pcs.add(pc)
        elif pc.connectionState == "connecting":
            print(f'{pc} is connecting.')
        elif pc.connectionState == "closed":
            await pc.close()
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is closed.')
        elif pc.connectionState == "failed":
            WebRTCStore.pcs.discard(pc)
            print(f'{pc} is failed.')


    @pc.on("datachannel")
    def on_datachannel(channel: RTCDataChannel):
        print(channel, "-", "created by remote party")
        @channel.on("message")
        async def on_message(message):
            if(message == "close"):
                await pc.close()

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        print("ICE State: ", pc.iceConnectionState)
        discard_state = ["closed", "failed"]
        if pc.iceConnectionState in discard_state:
            WebRTCStore.pcs.discard(pc)
            
    @pc.on("track")
    def on_track(track):
        if track.kind == "audio":
            pc.addTrack(
                AudioTransformTrack(track)
            )

    # handle offer
    await pc.setRemoteDescription(offer)
    answer = await pc.createAnswer()

    await pc.setLocalDescription(answer)

    return {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}
