
from typing import Set
from aiortc import RTCPeerConnection

class WebRTCStore:
    pcs: Set[RTCPeerConnection] = set()
