FROM python:3.9.7-slim-buster

WORKDIR /work

COPY . /work

RUN pip3 install -r requirements.txt

RUN pip3 install "uvicorn[standard]"

RUN pip3 install Jinja2 --upgrade

# RUN apt-get update
# RUN apt-get install ffmpeg libsm6 libxext6  -y

RUN apt-get update && apt-get install -y  --fix-missing \
    # ffmpeg \
    libavformat-dev \
    libavcodec-dev \
    libavdevice-dev \
    libavutil-dev 
    # libswscale-dev \
    # libswresample-dev \
    # libavfilter-dev

RUN pip install  -i https://pypi.tuna.tsinghua.edu.cn/simple av


EXPOSE 3000

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "3000"]

# launch with ssl
# sudo docker run -d --restart always -v /home/jihung/services/ssl_gen/cert.pem:/work/cert.pem -v /home/jihung/services/ssl_gen/key.pem:/work/key.pem --net host webrtc_demo:v0.1 uvicorn main:app --host 0.0.0.0 --port 44403 --ssl-keyfile /work/key.pem --ssl-certfile /work/cert.pem